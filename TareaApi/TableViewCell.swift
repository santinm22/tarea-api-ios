//
//  TableViewCell.swift
//  TareaApi
//
//  Created by Carlos Rey on 2023-03-29.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var team1: UILabel!
    @IBOutlet weak var team2: UILabel!
    @IBOutlet weak var resultadoTeam1: UILabel!
    @IBOutlet weak var resultadoTeam2: UILabel!
    @IBOutlet weak var tituloCategory: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)	
        // Configure the view for the selected state
    }
    
}
