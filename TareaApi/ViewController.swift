//
//  ViewController.swift
//  TareaApi
//
//  Created by Santiago Nuñez on 2023-03-28.
//

import UIKit

class ViewController: UIViewController {

    var data = [ToDo]()
    @IBOutlet weak var musicas: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        musicas.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "item1")
        ferchingApiData(URL: "http://192.168.67.130:3000/matches") { Result in
            self.data = Result
            DispatchQueue.main.async {
                self.musicas.reloadData()
            }
        }
    }
    func ferchingApiData(URL url:String, completion: @escaping ([ToDo]) -> Void){
        let url = URL(string: url)
        let sesion = URLSession.shared
        let dataTask = sesion.dataTask(with: url!) {data, response, error in
            if data != nil  && error == nil {
                do {
                    let parsingData = try JSONDecoder().decode([ToDo].self, from: data!)
                    completion(parsingData)
                }catch {
                    print("Parsing error")
                }
        
            }
        }
        dataTask.resume()
    }
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = musicas.dequeueReusableCell(withIdentifier: "item1", for: indexPath) as! TableViewCell
//        let cell2 = tableView.dequeueReusableCell(withIdentifier: "item2")
//        cell2?.textLabel?.text = data[indexPath.row].team2
//        cell.textLabel?.text = data[indexPath.row].team1
        
            var list = [Character]()
            data[indexPath.row].team1.forEach{ char in
                list.append(char)
            }
            var nueva = ""
            
            for i in 0...2{
                nueva+=String(list[i])
            }
        
        cell.team1.text = nueva
        
        list = [Character]()
        data[indexPath.row].team2.forEach{ char in
            list.append(char)
        }
        nueva = ""
        
        for i in 0...2{
            nueva+=String(list[i])
        }
        
        cell.team2.text = nueva
        cell.tituloCategory.text = data[indexPath.row].category
        
        
//        cell.resultadoTeam1.text = String(data[indexPath.row].concededteam1)
//        cell.resultadoTeam2.text = String(data[indexPath.row].concededteam2)
        return cell
    }
}
