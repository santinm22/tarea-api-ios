//
//  MODEL.swift
//  TareaApi
//
//  Created by Carlos Rey on 2023-03-28.
//

import Foundation

struct ToDo:Decodable {
    let team1: String
    let team2: String
//    let goals_team1: Int
//    let goals_team2: Int
    let category: String
    
//    private enum CodingKeys: String, CodingKey {
//        case team1
//        case team2
//        case goals_team1 = "conceded team1"
//        case goals_team2 = "conceded team2"
//        case category
//    }
}
